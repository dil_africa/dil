<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionSection extends Model
{
    public $table = 'promotion_sections';

    public $fillable = ['name', 'no_of_images'];

}