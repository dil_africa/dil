<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    //

    public $table = 'product_prices';

    public $guarded = ['id'];

    public $fillable = [
        'product_id',
        'minimum_quantity',
        'standard_price',
        'offer_price',
        'start_date',
        'end_date',
        'color',
        'size',
        'quantity',
        'offer_quantity',
        'is_default',
        'item_size_id'
    ];


    public function product()
    {
        return $this->BelongsTo('App\Product');
    }

    public function item_size()
    {
        return $this->BelongsTo('App\ItemSize');
    }


    public function images()
    {
        return $this->HasMany('App\ProductImage');
    }


    public function getProductPriceImages()
    {

        $images = ProductImage::where("product_price_id", $this->id)->get();

        return $images;
    }
}